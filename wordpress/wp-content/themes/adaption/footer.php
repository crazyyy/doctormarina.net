<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package adaption
 */
?>


    <footer id="colophon" class="site-footer" role="contentinfo">

      <div class="site-info">
        <a href="http://doctormarina.net/">Доктор Марина Невдовец — профессиональный косметолог, Киев</a>
      </div><!-- .site-info -->

    </footer><!-- #colophon .site-footer -->

  </div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
